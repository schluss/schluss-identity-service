"use strict";

const http = require('http');
const https = require('https');

class Request {
	
	get(url, options) {
		
		if (!options)
			options = {};
		
		options.method = 'GET';
		
		return this.request(url, options);
		

	}
	
	post(url, options) {
		
		if (!options)
			options = {};
		
		options.method = 'POST';
		
		return this.request(url, options);
		
	}
		
	request(url, options) {
		return new Promise((resolve, reject) => {
			
			let requestObj;
			
			// auto switch between requestobject
			if (url.includes('https')){
				requestObj = https;
			} 
			else {
				requestObj = http;
			}
			
			const req = requestObj.request(url, options, (res) => {
				res.setEncoding('utf8');
				
				let responseBody = '';

				res.on('data', (chunk) => {
					responseBody += chunk;
				});

				res.on('end', () => {
					resolve(responseBody);
				});
			});
			
			if (options.json)
				req.setHeader('Content-Type', 'application/json');	

			req.on('error', (err) => {
				reject(err);
			});

			if (options && options.body) {
				
				if (typeof options.body === 'object'){
					options.body = JSON.stringify(options.body);
				}
				
				req.write(options.body);
			}
			
			req.end();
		});
	}
	
	
}

module.exports = new Request();