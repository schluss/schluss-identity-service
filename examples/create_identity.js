const { KeyType, KeyPair, Document, publish, Client, Config, Network } = require('@iota/identity-wasm/node');
const request = require('./request.js');

/*
 SETUP
*/

const API_HOST = process.env.HOST ? process.env.HOST : 'http://localhost:' + process.env.PORT + '/';
const API_KEY = process.env.API_KEY;

/* 
	CREATE AN IDENTITY

	Publish and create an Identity (DID). 
*/

async function run(){

	
	const Nw = process.env.IOTA_NETWORK == 'main' ? Network.mainnet() : Network.testnet();
		
	// Create Key and DID Document
	const { doc, key } = new Document(KeyType.Ed25519, Nw.toString()); // Nw needed here?

	// Sign document with key
	doc.sign(key);

    // Create a default client configuration from the parent config network.
    const config = Config.fromNetwork(Nw);
	//config.setNodeSyncDisabled();
	//config.setNetwork(Nw);
	config.setNode(Nw.defaultNodeURL);
	
	// When using mainnet, also use the perma node
	if (process.env.IOTA_NETWORK == 'main'){
		config.setPermanode('https://chrysalis-chronicle.iota.org/api/mainnet/');
	}
    // Create a client instance to publish messages to the Tangle.
    const client = Client.fromConfig(config);

	// Publish DID Document
    // Publish the Identity to the IOTA Network, this may take a few seconds to complete Proof-of-Work.
    const messageId = await client.publishDocument(doc.toJSON());

	console.log('Key:');
	console.log(key);
	
	console.log('DID:');
	console.log(doc.id.toString());	
	
	console.log('Explore:');
	//console.log(Nw.messageURL(messageId)); // does not work yet
	console.log(process.env.IOTA_EXPLORER_URL + '/' + messageId	);	

	console.log('Resolved?:');
	console.log(await client.resolve(doc.id.toString()));
}

run();
