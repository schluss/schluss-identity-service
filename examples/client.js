const { Document, KeyType, KeyPair, VerificationMethod, VerifiablePresentation } = require('@iota/identity-wasm/node');
const request = require('./request.js');

/*
 SETUP
*/

const EMAIL_ADDRESS = 'your@email.com';
const API_HOST = process.env.HOST ? process.env.HOST : 'http://localhost:' + process.env.PORT + '/';
const API_KEY = process.env.API_KEY;

/* 
	CLIENT EXAMPLE

	Using a created identity, you will validate your email address. Validation 
	results in a Verifiable credential, which is then shared to a third party
	in the form of a Verifiable presentation. The third party then verifies
	the Vp

	Before running this, make sure the api service is setup and running!

*/

async function run(){

	// CREATING AN IDENTITY

	// Create a DID Document (an identity)
	const { doc, key } = new Document(KeyType.Ed25519);
	
	console.log('client identity:');
	console.log(key);

	// Sign DID Document with key
	doc.sign(key);
	
	// Publish DID Document
	const DIDResult = JSON.parse(await request.post(API_HOST + 'identities', {
		body: doc.toJSON(),
		headers: { authorization : API_KEY},
		json:true
	}));

		
	// START E-MAIL VERIFICATION REQUEST
	
	const VerifyResult = JSON.parse(await request.post(API_HOST + 'request', {
		body: { did :  doc.id.toString(), email : EMAIL_ADDRESS},
		headers: { authorization : API_KEY },
		json:true
	}));
	
	//console.log(JSON.parse(VerifyResult).verificationId);
	
	let isVerified = false
	let signedVc = null;
		
	console.log('Identity created, verification request started. Open the verification email in your inbox. After clicking on the link in the mail, this continues');
	
	process.stdout.write('polling');
	while(!isVerified){
			
		process.stdout.write('.');
		
		await sleep(3000);
		
		// poll status
		let statusResult = JSON.parse(await request.get(API_HOST + 'request/' + VerifyResult.requestId, {
			headers: { authorization : API_KEY},
			json:true
		}));	
	
		// when verified, end loop
		if (statusResult && statusResult.verificationStatus == 'verified'){
			console.log('');
			isVerified = true;
			signedVc = statusResult.verifiableCredential;			
		}
	}
	
	// We have a signed VC
	console.log(signedVc);
	
	// FROM THE VC, GENERATE AND SIGN A VP
	const unsignedVp = new VerifiablePresentation(doc, signedVc);
	
    const signedVp = doc.signPresentation(unsignedVp, {
        method: "#key",
        secret: key.secret,
    });
	
	console.log('signedVP:');
	console.log(signedVp.toJSON());
	
	// VERIFIER VERIFIES THE SIGNED VP
		
	const VpVerifyResult = JSON.parse(await request.post(API_HOST + 'verify/vp', {
		body: signedVp.toJSON(),
		headers: { authorization : API_KEY },
		json:true
	}));	
	
	console.log('VP verification result:');
	console.log(VpVerifyResult);
}

async function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

run();

