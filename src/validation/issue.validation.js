const Joi = require('joi');

const create = {
  body: Joi.object().keys({
    did: Joi.string().required(),
	attributes: Joi.array().items({
		name: Joi.string().required(),
		value: Joi.required()    
    })
  }),
};


module.exports = {
	create
};
