const Joi = require('joi');

const create = {
  body: Joi.object().keys({
    did: Joi.string().required(),
	email:Joi.string().email().required()
  }),
};

const getRequest = {
  params: Joi.object().keys({
    requestId: Joi.string().required(),
    key: Joi.string().required(),
  }),
};

const cancelRequest = {
  params: Joi.object().keys({
    requestId: Joi.string().required(),
	key: Joi.string().required(),
  }),
};

module.exports = {
	create,
	getRequest,
	cancelRequest
};
