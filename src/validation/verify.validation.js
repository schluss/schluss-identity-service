const Joi = require('joi');

const verify = {
  body: Joi.object().keys({
	'@context': Joi.string().required(),
  	type: Joi.required(),
 	verifiableCredential : Joi.object().keys({
		'@context' : Joi.string().required(),
		id : Joi.string().required(),
		type : Joi.array().required(),
		credentialSubject:Joi.required(),
		/*credentialSubject: Joi.object().keys({
			id : Joi.string().required(),
			email : Joi.string().email().required(),
		}),*/
		issuer: Joi.string().required(),
		issuanceDate: Joi.string().required(),
		proof : Joi.object().keys({
			type : Joi.string().required(),
			verificationMethod : Joi.string().required(),
			signatureValue : Joi.string().required()
		})
	}),
	holder: Joi.string().required(),
	proof : Joi.object().keys({
		type : Joi.string().required(),
		verificationMethod : Joi.string().required(),
		signatureValue : Joi.string().required()
	})
  }),
};


module.exports = {
	verify
};
