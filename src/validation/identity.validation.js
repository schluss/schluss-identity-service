const Joi = require('joi');

const create = {
  body: Joi.object().keys({
    id: Joi.string().required(),
    authentication: Joi.array().items({
		id: Joi.string().required(),
		controller: Joi.string().required(),
		type: Joi.string().required(),
		publicKeyBase58: Joi.string().required(),
      
    }),
    created: Joi.string().required(),
    updated: Joi.string().required(),
	proof : Joi.object().keys({
		type : Joi.string().required(),
		verificationMethod : Joi.string().required(),
		signatureValue : Joi.string().required()
	})
  }),
};

const resolve = {
  params: Joi.object().keys({
    did: Joi.string().required(),
  }),
};

module.exports = {
  create,
  resolve,
};
