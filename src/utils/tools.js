"use strict";

const crypto = require('crypto');

module.exports = { 
	
	// generate a random hash
	// note, length must be an even number
	hash : (length) => {
		
		if (length %2 == 1)
			throw 'Invalid hash length';
		
		return crypto.randomBytes(length/2).toString('hex');
	},
	
	encrypt : (text, key) => {
		
		const iv = crypto.randomBytes(16);
		
		const cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
		const encrypted = cipher.update(text);
		const encryptedBuffer = Buffer.concat([encrypted, cipher.final()]);
		
		return JSON.stringify({ 
			iv: iv.toString('hex'), 
			encryptedData: encryptedBuffer.toString('hex')
		});
	},

	decrypt : function (text, key) {
		text = JSON.parse(text);
		const iv = Buffer.from(text.iv, 'hex');
		const encryptedText = Buffer.from(text.encryptedData, 'hex');
		const decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
		const decrypted = decipher.update(encryptedText);
		const decryptedBuffer = Buffer.concat([decrypted, decipher.final()]);
		
		return decryptedBuffer.toString();
	}	
}