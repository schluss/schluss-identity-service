"use strict";

const nodemailer = require('nodemailer');
const storage =  require('../services/storage.service');
const tools = require('../utils/tools.js');
const path = require("path");
var hbs = require('express-handlebars').create();

const RequestController = {
	
	/* 

	**/
	createRequest : async (req, res, next) => {
		
		try {
			const did = req.body.did;				
			const email = req.body.email;				

			const token = tools.hash(32);
			const key = tools.hash(32);
			
			const body = {
				token : token,
				did : did,
				email : email,
				time : Date.now(),
				status : 'pending'
			};
			
			// encrypt body before storing
			const encBody = tools.encrypt(JSON.stringify(body), key);
			
			const objectId = await storage.store(encBody);				
			
			// send mail

			// get the host (https://url.to.api)
			let verificationUrl = process.env.HOST ? process.env.HOST : req.protocol + '://' + req.get('host');
			verificationUrl += '/validate/email/' + objectId + '/' + token + '/' + key;

			let textMail = await hbs.render(path.resolve(__dirname + '/../views/email/mailbody.txt'), {
					verificationUrl : verificationUrl
				});
			
			let htmlMail = await hbs.render(path.resolve(__dirname + '/../views/email/mailbody.html'), {
					verificationUrl : verificationUrl
				});
			
			// create reusable transporter object using the default SMTP transport
			let transporter = nodemailer.createTransport({
				host: process.env.MAIL_HOST,
				port: process.env.MAIL_PORT,
				secure: process.env.MAIL_SECURE == 'false' ? false : true, // true for 465, false for other ports
				auth: {
					user: process.env.MAIL_USER,
					pass: process.env.MAIL_PASSWORD,
				},

			});
			
			// send mail with defined transport object
			await transporter.sendMail({
				from: process.env.MAIL_FROM,
				to: email,
				subject: process.env.MAIL_SUBJECT,
				text: textMail,
				html: htmlMail,
			});		
			
			return res.message(200,{
				requestId : objectId + '/' + key
			});		
		}
		catch(err){
			return next(err);
		}		
	},
	
	/* 
	 * Check request verification status, return vc
	 * header Authorization API_KEY
	**/
	getRequest : async (req, res, next) => {
		
		try {
			
			const requestId = req.params.requestId;			
			const key = req.params.key;			
						
			// check if object exists
			if (!requestId || !await storage.exists(requestId)){
				return res.message(404,'Verification request not found');	
			}
			
			// get the encrypted object
			const objectEncrypted = await storage.get(requestId);
			
			// get the object as json
			const objectJson = JSON.parse(tools.decrypt(objectEncrypted, key));
			
			// verification status can only be requested once, so when verified: delete it
			if (objectJson.status == 'verified'){
				storage.delete(requestId);
				
				// dirty hack to switch between a single vc or a 'list of vc's 
				let vc;
				
				if (objectJson.vc){
					vc = objectJson.vc;
				}
				else {
					
					let vcs = [];
					for (let attr in objectJson.attributes){
						vcs.push({
							name : objectJson.attributes[attr].name,
							vc : objectJson.attributes[attr].vc
						});
					}
					vc = vcs;
				}
				
				return res.message(201, {
					verificationStatus : objectJson.status,
					verifiableCredential : vc
				});
			}
			else {
				return res.message(200, {
					verificationStatus : objectJson.status,
					verificationMessage: objectJson.error
				});
			}

		}
		catch(err){
			return next(err);
		}		
	},
	
	/* 
	 * Cancel and delete a request
	 * 
	**/
	cancelRequest : async (req, res, next) => {
		try {
			
			const requestId = req.params.requestId;			
						
			// check if object exists
			if (!requestId || !await storage.exists(requestId)){
				return res.message(404,'Verification request not found');	
			}
			
			storage.delete(requestId);
			
			return res.message(200, 'Verification request deleted');

		}
		catch(err){
			return next(err);
		}
	},		
};


module.exports = RequestController;
