"use strict";

const IOTAService = require('../services/iota.service');

const IdentityController = {
	
	/* 
		Publish a new Identity
	**/
	postIdentity : async (req, res, next) => {
		
		try {		
	
			// Publish the Identity to the IOTA Network, this may take a few seconds to complete Proof-of-Work.
			const result = await IOTAService.publishDID(req.body);
			
			return res.message(200, result);			
		}
		catch(err){
			return next(err);
		}
	},		
	
	/* 
		Resolve an existing identity
	**/
	resolveIdentity : async (req, res, next) => {
		
		try {
			
			// Resolve the given did
			const result = await IOTAService.resolveDID(req.params.did);
			
			return res.message(200, { didDocument : result });
		}
		catch(err){
			return next(err);
		}		
	},
	

};


module.exports = IdentityController;
