"use strict";

const nodemailer = require('nodemailer');
const storage =  require('../services/storage.service');
const tools = require('../utils/tools.js');
const path = require("path");
var hbs = require('express-handlebars').create();
const IOTAService = require('../services/iota.service');

const RequestController = {
	
	/* 

	**/
	createIssue : async (req, res, next) => {
		
		try {
			const did = req.body.did;				
			const attributes = req.body.attributes;				

			const token = tools.hash(32);
			const key = tools.hash(32);
			
			const body = {
				token : token,
				did : did,
				attributes : attributes,
				time : Date.now(),
				status : 'pending'
			};
			
			// encrypt body before storing
			const encBody = tools.encrypt(JSON.stringify(body), key);
			
			const objectId = await storage.store(encBody);				
			
			// Start issueing processing
			processIssueing(attributes, objectId, key);
			
			
			// Return the requestId so it is possible to get the status updates
			
			return res.message(200,{
				requestId : objectId + '/' + key
			});		
		}
		catch(err){
			return next(err);
		}		
	},
	
	/* 
	 * Cancel and delete an issueing request
	 * 
	**/
	cancelIssue : async (req, res, next) => {
		try {
			
			const requestId = req.params.requestId;			
						
			// check if object exists
			if (!requestId || !await storage.exists(requestId)){
				return res.message(404,'Verification request not found');	
			}
			
			storage.delete(requestId);
			
			return res.message(200, 'Verification request deleted');

		}
		catch(err){
			return next(err);
		}
	},		
};

async function processIssueing(attributes, requestId, key) {
	
	try {
	
		// Get the requestId content:
		
		// check if it exists
		if (!await storage.exists(requestId)){
			return;	
		}
		
		// get the encrypted object
		const objectEncrypted = await storage.get(requestId);
		
		// decrypt and convert to json
		const objectJson = JSON.parse(tools.decrypt(objectEncrypted, key));			
				
		let isCancelled = false; 
		
		// run parrallel executing of the individual attribute validation
		await Promise.all(attributes.map(async (attr, index) => {
			
			let name = attr.name;
			
			// when cancelled, halt further execution
			if (isCancelled)
				return;
			
			// check if request still exists, if not, we cancel further execution
			if (!await storage.exists(requestId)){
				isCancelled = true;
				return;
			}
			
			// create credential to sign
			let credential = {
				id: 'http://schluss.app/credentials/' + name.toLowerCase(),
				type: name + 'Credential',
				credentialSubject : {
					id : objectJson.did,
				},
			}		
			credential.credentialSubject[name] = attr.value;
						
			// Create and sign the credential
			const signedVc = await IOTAService.signCredential(credential);
			
			// add the credential to the attributes
			objectJson.attributes[index].name = name;	
			objectJson.attributes[index].vc = signedVc;	
		}));		
		
		// all done, finishing up
		
		if (isCancelled){
			return;
		}
			
				
		// update status
		objectJson.status = 'verified';
				
		// encrypt body before storing
		const encBody = tools.encrypt(JSON.stringify(objectJson), key);
		
		// update object
		await storage.update(requestId, encBody);
	}
	catch(err){
		throw(err);
		// get the encrypted object
		const objectEncrypted = await storage.get(requestId);
		
		// decrypt and convert to json
		const objectJson = JSON.parse(tools.decrypt(objectEncrypted, key));
		
		objectJson.status = 'failed';
		objectJson.error = err;

		// encrypt body before storing
		const encBody = tools.encrypt(JSON.stringify(objectJson), key);
		
		// update object
		await storage.update(requestId, encBody);
	}
}


module.exports = RequestController;
