"use strict";

const IOTAService = require('../services/iota.service');

const VerifyController = {
	
	postVerifyVP : async (req, res, next) => {
		try {		

			const result = await IOTAService.checkPresentation(req.body);
			
			return res.message(200, { verified : result.verified });	
		}
		catch(err){
			return next(err);
		}			
	},
	
	postVerifyVC : async (req, res, next) => {
		try {		

			const result = await IOTAService.checkCredential(req.body);
			
			return res.message(200, { verified : result.verified });	
		}
		catch(err){
			return next(err);
		}			
	},	
};

module.exports = VerifyController;
