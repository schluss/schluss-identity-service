"use strict";

const logger = require('../utils/logger.js');
const tools = require('../utils/tools.js');
const storage = require('../services/storage.service');
const IOTAService = require('../services/iota.service');

const ValidateController = {
	
	/*
	 * Verifiy e-mail address, resulting in a nice HTML page being displayed to the visitor
	 *
	 */
	validateEmail : async (req, res) => {
		
		try {
			
			const requestId = req.params.requestId;
			const token = req.params.token;
			const key = req.params.key;
		
			if (!requestId || !token || !key){
				return res.render('error', {
					error : 'Invalid url'
				});				
			}
			
			// check if object exists
			if (!await storage.exists(requestId)){
				return res.render('error', {
					error : 'Verification not found'
				});					
			}
			
			// get the encrypted object
			const objectEncrypted = await storage.get(requestId);
			
			// decrypt and convert to json
			const objectJson = JSON.parse(tools.decrypt(objectEncrypted, key));
			
			// check token
			if (objectJson.token != token){
				return res.render('error', {
					error : 'Invalid token'
				});				
			}

			// process the verification in an async way
			processValidation(requestId, objectJson, key);
			
			return res.render('email/validated', {
				title : 'Email gevalideerd'
			});

		}
		catch(err){
			// log the error
			logger.log(err);

			// show nice error page to visitor
			return res.render('error', {
				error : err
			});				
		}		
	},
	
	validatePhonenumber : async (req, res, next) => {
		
		try {
			
			throw('Not implemented');
		}
		catch(err){
			return next(err);
		}		
	},
};


async function processValidation(requestId, objectJson, key) {
	
	try {
	
		const credential = {
			id: "http://schluss.app/credentials/email",
			type: "EmailCredential",
			credentialSubject : {
				id : objectJson.did,
				email : objectJson.email
			},
		}
		
		// Create and sign the credential
		const signedVc = await IOTAService.signCredential(credential);
			
		// update status
		objectJson.status = 'verified';
		objectJson.vc = signedVc;
		
		// encrypt body before storing
		const encBody = tools.encrypt(JSON.stringify(objectJson), key);
		
		// update object
		await storage.update(requestId, encBody);
	}
	catch(err){
		objectJson.status = 'failed';
		objectJson.error = err;

		// encrypt body before storing
		const encBody = tools.encrypt(JSON.stringify(objectJson), key);
		
		// update object
		await storage.update(requestId, encBody);
	}
}


module.exports = ValidateController;
