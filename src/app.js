"use strict";

const express = require('express');
const helmet = require('helmet');
const routes = require('./routes');
const logger = require('./utils/logger.js');
const exphbs = require('express-handlebars');
const cors = require('./middleware/cors.middleware.js');
const path = require('path');

const app = express();

// setup CORS
app.use(cors);

// secure against different types of attacks
app.use(helmet());

// define a generic response message
express.response.message = function (httpStatus, message, detail){
	
	this.setHeader('Content-Type', 'application/json');
	
	let msg = {
		timestamp : new Date().toString(),
		status : httpStatus
	};

	// when message is an object (like a returned resource), display that instead of 'wrapping' it in a message property
	if (typeof message === 'object' && message !== null){
		msg = Object.assign(msg, message);
	}
	else {
		msg.message = message;
	}		
	
	// if detail information is available
	if (detail){
		msg.detail = detail;
	}
	
	return this.status(httpStatus).json(msg);
}

// log all requests
app.use((req, res, next) => {
	logger.log('['+ req.method + '] ' + req.url);
	return next();
});	

// set the views location
	app.set('views', './src/views');

// template engine
	app.engine('html', exphbs({
		extname: '.html',
		defaultLayout : 'main'
	}));
	app.set('view engine', 'html');


// connect static assets 
app.use(express.static(path.join(__dirname, 'static')));

// connect routes
app.use('/', routes);


// log errors
app.use(logger.expressLog);
	
// handle errors
app.use(function (err, req, res, next) {
	
	if (res.headersSent) {
		return next(err);
	}
  
	return res.message(500, err.message);
});
	
module.exports = app;