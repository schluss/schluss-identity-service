"use strict";

const authorize = (req, res, next) => {
	
	const apikey = req.headers['x-access-token'] || req.headers.authorization;
		
	if (!apikey || apikey != process.env.API_KEY) {
		return res.message(401,'Access denied', 'Incorrect credentials');
	}
	else {
		return next();	
	}
};

module.exports = authorize;