"use strict";

const app = require('./app');
const logger = require('./utils/logger');

let server;

server = app.listen(process.env.PORT, () => {
	logger.log('Schluss Verification Service listening at http://localhost:' + process.env.PORT, 'info', true);
});

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.log('Server closed', 'info', true);
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error) => {
  logger.log(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.log('SIGTERM received', 'info', true);
  if (server) {
    server.close();
  }
});
