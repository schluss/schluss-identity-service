"use strict";

const storage = require('./storage.' + process.env.STORAGE_PROVIDER + '.js');

// if storage adapter contains init function:
if (storage['init'] instanceof Function){
	console.log('storage adapter, initializing');
	storage.init();
}

console.log('storage adapter loaded: ' + process.env.STORAGE_PROVIDER);

module.exports = storage;