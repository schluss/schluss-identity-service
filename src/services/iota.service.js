"use strict";

const { KeyPair, KeyType, Document, VerifiableCredential, checkPresentation, Config, Network, Client } = require('@iota/identity-wasm/node');

const IOTAService = {
	
	// Publish a new DID
	publishDID : async (document) => {
		
		const Nw = process.env.IOTA_NETWORK == 'main' ? Network.mainnet() : Network.testnet();
		
		// Create a client configuration
		const config = Config.fromNetwork(Nw);
		//config.setNodeSyncDisabled();
		//config.setNetwork(Nw);
		config.setNode(Nw.defaultNodeURL);
		
		// When using mainnet, also use the perma node to store the did permanently
		if (process.env.IOTA_NETWORK == 'main'){
			config.setPermanode('https://chrysalis-chronicle.iota.org/api/mainnet/');
		}		
		
		// Create a client instance to publish messages to the Tangle.
		const client = Client.fromConfig(config);
	
		const messageId = await client.publishDocument(document);
				
		return {
			messageId : messageId,
			location : process.env.IOTA_EXPLORER_URL + '/' + messageId			
		}
	},
	
	resolveDID : async (did) => {
		
		const Nw = process.env.IOTA_NETWORK == 'main' ? Network.mainnet() : Network.testnet();
		
		// Create a client configuration
		const config = Config.fromNetwork(Nw);
		//config.setNodeSyncDisabled();
		//config.setNetwork(Nw);
		config.setNode(Nw.defaultNodeURL);
		
		// When using mainnet, also use the perma node to store the did permanently
		if (process.env.IOTA_NETWORK == 'main'){
			config.setPermanode('https://chrysalis-chronicle.iota.org/api/mainnet/');
		}		
		
		// Create a client instance to publish messages to the Tangle.
		const client = Client.fromConfig(config);		
		
		const result = await client.resolve(did);
		
		return result.document;
	},
	
	signCredential : async (credential) => {
		
		// create Issuer Key Object
		const issuerKey = await KeyPair.fromBase58(KeyType.Ed25519, process.env.ISSUER_PUBLIC, process.env.ISSUER_SECRET);
				
		// Get Issuer Identity
		const issuerDID = await IOTAService.resolveDID(process.env.ISSUER_DID);
					
		// Generate DID Document from resolved Issuer DID
		const DIDDoc = await Document.fromJSON(issuerDID); 	
				
		// Add issuer DID to credential
		credential.issuer = DIDDoc.id.toString();

		// Create an unsigned credential
		const unsignedVc = VerifiableCredential.extend(credential);		

		// Sign the credential with the issuers key
		const signedVc = DIDDoc.signCredential(unsignedVc, {
			method: DIDDoc.id.toString() + '#key',
			public: issuerKey.public,
			secret: issuerKey.secret,		
		});	
		
		return signedVc.toJSON();
	
	},
	
	checkPresentation : async (verifiablePresentation) => {
		
		const Nw = process.env.IOTA_NETWORK == 'main' ? Network.mainnet() : Network.testnet();
		
		// Create a client configuration
		const config = Config.fromNetwork(Nw);
		//config.setNodeSyncDisabled();
		//config.setNetwork(Nw);
		config.setNode(Nw.defaultNodeURL);
		
		// When using mainnet, also use the perma node to store the did permanently
		if (process.env.IOTA_NETWORK == 'main'){
			config.setPermanode('https://chrysalis-chronicle.iota.org/api/mainnet/');
		}	

		// Create a client instance to publish messages to the Tangle.
		const client = Client.fromConfig(config);				
		
		const result = await client.checkPresentation(JSON.stringify(verifiablePresentation));
		
		return result;
	},
	
	checkCredential : async (verifiableCredential) => {
		
		const Nw = process.env.IOTA_NETWORK == 'main' ? Network.mainnet() : Network.testnet();
		
		// Create a client configuration
		const config = Config.fromNetwork(Nw);
		//config.setNodeSyncDisabled();
		//config.setNetwork(Nw);
		config.setNode(Nw.defaultNodeURL);
		
		// When using mainnet, also use the perma node to store the did permanently
		if (process.env.IOTA_NETWORK == 'main'){
			config.setPermanode('https://chrysalis-chronicle.iota.org/api/mainnet/');
		}	

		// Create a client instance to publish messages to the Tangle.
		const client = Client.fromConfig(config);				
		
		const result = await client.checkCredential(JSON.stringify(verifiableCredential));
		
		return result;
	},
	
}


module.exports = IOTAService;