"use strict";

const { v4: uuidv4 } = require('uuid');
const nestedObjectAssign  = require('nested-object-assign');
const AWS = require('aws-sdk');

// Storage adapter providing S3 compatible storage

exports.name = 's3';

const spacesEndpoint = new AWS.Endpoint(process.env.S3_ENDPOINT_URL); // Replace with the endpoint for your region.
const s3 = new AWS.S3({
	endpoint: spacesEndpoint,
	accessKeyId: process.env.S3_ACCESS_KEY_ID,
	secretAccessKey: process.env.S3_ACCESS_KEY_SECRET
});
const Bucket = process.env.S3_BUCKET_NAME;

exports.store = async function(content){
	return new Promise((resolve, reject) => {

		const objectId = uuidv4();

		const params = {
			Bucket: Bucket,
			Key: objectId,	
			Body : content
		};

		s3.putObject(params, function(err) {
			if (err) { 
				return reject(err);
			}
			
			return resolve(objectId);
		});
	});
}

// not sure if this one will stay, 
// maybe it's a too costly process to first check if the object exists and then return it.
// we might just immediately try to get it!
exports.exists = async function(objectId){
	return new Promise((resolve, reject) => {
		
		const params = {
			Bucket: Bucket,
			Key: objectId
		};

		s3.headObject(params, function(err) {
			if (err) { 
			
				if (err.code.toLowerCase() == 'notfound') {
					return resolve(false);
				}
				
				return reject(err);
			}
			
			return resolve(true);
		});
	});
}

exports.get = async function(objectId){
	return new Promise(async (resolve) => {
		
		if (!await exports.exists(objectId)){
			return resolve('');
		}
		
		const params = {
			Bucket: Bucket,
			Key: objectId
		};		
		
		s3.getObject(params, function(err, data) {
			if (err){
				return resolve('');
			}
			
			return resolve(data.Body.toString('utf8'));
		});
	});
}

exports.delete = async function(objectId){
	return new Promise(async (resolve, reject) => {
		
		const params = {
			Bucket: Bucket,
			Key: objectId
		};			
		
		s3.deleteObject(params, function(err) {
			if (err) {
				return reject(err);
			}
			else {
				resolve(true); 
			}
		});
	});
}

exports.patch = async function(objectId, content){
	return new Promise(async (resolve, reject) => {
	
		if (!await exports.exists(objectId)){
			return reject(false);
		}
		
		const object = await exports.get(objectId);
		let objectJson = JSON.parse(object);
		
		const mergedObject = nestedObjectAssign({}, objectJson, content);
		
		const params = {
			Bucket: Bucket,
			Key: objectId,	
			Body : JSON.stringify(mergedObject)
		};

		s3.putObject(params, function(err) {
			if (err) { 
				return reject(err);
			}
			
			return resolve(objectId);
		});
	
	});
}

exports.update = async function(objectId, content){
	return new Promise(async (resolve, reject) => {
	
		if (!await exports.exists(objectId)){
			return reject(false);
		}
		
		const params = {
			Bucket: Bucket,
			Key: objectId,	
			Body : content
		};

		s3.putObject(params, function(err) {
			if (err) { 
				return reject(err);
			}
			
			return resolve(objectId);
		});
	
	});
}