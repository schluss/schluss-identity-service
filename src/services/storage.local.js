"use strict";

const fs = require('fs');
const path = require('path');
const { v4: uuidv4 } = require('uuid');
const logger = require('../utils/logger.js');
const nestedObjectAssign  = require('nested-object-assign');

// Storage adapter providing local file storage

exports.name = 'local';

const storagePath = process.env.LOCAL_STORAGE_PATH;

exports.init = async function(){
	
	// enable autodeletion of remainder files
	const autoDeleteEnabled = process.env.STORAGE_AUTODELETE == 'true' ? true : false;
	const autoDeleteExpiry = process.env.STORAGE_AUTODELETE_EXPIRY;
	const autoDeleteInterval = process.env.STORAGE_AUTODELETE_INTERVAL;


	if (autoDeleteEnabled){
		function walkDir(dir, callback) {
		  fs.readdirSync(dir).forEach( f => {
			let dirPath = path.join(dir, f);
			let isDirectory = fs.statSync(dirPath).isDirectory();
			isDirectory ? 
			  walkDir(dirPath, callback) : callback(path.join(dir, f));
		  });
		};	
		
		setInterval(function() {
			
			logger.log('Run autodelete interval');
			
			walkDir(storagePath, function(filePath) {
			fs.stat(filePath, function(err, stat) {
			var now = new Date().getTime();
			var endTime = new Date(stat.mtime).getTime() + (86400000 * autoDeleteExpiry); // miliseconds 1 day * days

			if (err) { return logger.log(err); }

			if (now > endTime) {
				
				logger.log('Delete:' + filePath);
				
				return fs.unlink(filePath, function(err) {
					if (err) return console.error(err);
				});
			}
		  })  
		});
		}, (86400000 * autoDeleteInterval)); // miliseconds 1 hr * interval hours
	}


}

exports.store = async function(content){
	return new Promise((resolve, reject) => {

		const objectId = uuidv4();

		fs.writeFile(storagePath + objectId, content, (err) => {
		  if (err) {
			  return reject(err);
		  }
		  
		  return resolve(objectId);
		  
		});

	});
}

// not sure if this one will stay, 
// maybe it's a too costly process to first check if the object exists and then return it.
// we might just immediately try to get it!
exports.exists = async function(objectId){
	return new Promise((resolve) => {
		
		fs.access(storagePath + objectId, fs.F_OK, (err) => {
		  if (err) {
			  return resolve(false);
		  }
		  
		  return resolve(true);
		  
		});
	});
}

exports.get = async function(objectId){
	return new Promise(async (resolve) => {
		
		if (!await exports.exists(objectId)){
			return resolve('');
		}
		
		fs.readFile(storagePath + objectId, 'utf8', (err,content) => {
			
			if (err){
				return resolve('');
			}
			
			return resolve(content);
		});

	});
}

exports.delete = async function(objectId){
	return new Promise(async (resolve, reject) => {
		fs.unlink(storagePath + objectId, (err) => {
			if (err){
				return reject(false);
			}
			
			return resolve(true);
		});
	});
}



exports.patch = async function(objectId, content){
	return new Promise(async (resolve, reject) => {
	
		if (!await exports.exists(objectId)){
			return reject(false);
		}
		
		const object = await exports.get(objectId);
		let objectJson = JSON.parse(object);
		
		const mergedObject = nestedObjectAssign({}, objectJson, content);
		
		fs.writeFile(storagePath + objectId, JSON.stringify(mergedObject), (err) => {
			if (err) {
				return reject(false);
			}
			  
			return resolve(true);
		});
	
	});
}

exports.update = async function(objectId, content){
	return new Promise(async (resolve, reject) => {
	
		if (!await exports.exists(objectId)){
			return reject(false);
		}
		
		fs.writeFile(storagePath + objectId, content, (err) => {
			if (err) {
				return reject(false);
			}
			  
			return resolve(true);
		});
	
	});
}



