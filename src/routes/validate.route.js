"use strict";

const router = require('express').Router();
const ValidateController  = require('../controllers/validate.controller');

router.get('/email/:requestId/:token/:key', ValidateController.validateEmail);
router.get('/phonenumber', ValidateController.validatePhonenumber);

module.exports = router;