"use strict";

const router = require('express').Router();
const RequestController  = require('../controllers/request.controller');
const authorize = require('../middleware/authorize.middleware');
const jsonParser = require('../middleware/jsonparser.middleware');
const validate = require('../middleware/validate.middleware');
const { create, getRequest, cancelRequest } = require('../validation/request.validation');

router.post('/', authorize, jsonParser, validate(create), RequestController.createRequest);
router.get('/:requestId/:key', authorize, validate(getRequest), RequestController.getRequest);
router.delete('/:requestId/:key', authorize, validate(cancelRequest), RequestController.cancelRequest);

module.exports = router;