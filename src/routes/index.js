"use strict";

const router = require('express').Router();


// define sub-routes
router.use('/identities', require('./identities.route'));
router.use('/verify', require('./verify.route'));
router.use('/request', require('./request.route'));
router.use('/issue', require('./issue.route'));
router.use('/validate', require('./validate.route'));
router.use('/docs', require('./docs.route'));


router.get('/', (req, res) =>{
	return res.message(200, 'Schluss Identity Service');
});

router.get('/favicon.ico', (req, res) => {res.status(204).end()});

// default route: when no route matches
router.get('*', function (req, res) {
	return res.message(400, 'Invalid route');
});

module.exports = router;