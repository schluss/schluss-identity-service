"use strict";

const router = require('express').Router();
const IssueController  = require('../controllers/issue.controller');
const authorize = require('../middleware/authorize.middleware');
const jsonParser = require('../middleware/jsonparser.middleware');
const validate = require('../middleware/validate.middleware');
const { create} = require('../validation/issue.validation');

router.post('/', authorize, jsonParser, validate(create), IssueController.createIssue);

module.exports = router;