"use strict";

const router = require('express').Router();
const IdentityController  = require('../controllers/identity.controller');
const authorize = require('../middleware/authorize.middleware');
const jsonParser = require('../middleware/jsonparser.middleware');
const validate = require('../middleware/validate.middleware');
const { create, resolve } = require('../validation/identity.validation');

router.post('/', authorize, jsonParser, validate(create), IdentityController.postIdentity);
router.get('/:did', authorize, validate(resolve), IdentityController.resolveIdentity);
//Todo: router.delete('/', authorize, IdentityController.revokeIdentity);

module.exports = router;