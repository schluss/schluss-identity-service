"use strict";

const router = require('express').Router();
const VerifyController = require('../controllers/verify.controller');
const jsonParser = require('../middleware/jsonparser.middleware');
const validate = require('../middleware/validate.middleware');
const { verify } = require('../validation/verify.validation');

router.post('/vp', jsonParser, validate(verify), VerifyController.postVerifyVP);
router.post('/vc', jsonParser, VerifyController.postVerifyVC);

module.exports = router;