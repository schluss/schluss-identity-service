FROM node:17

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
ADD ./package*.json ./
ADD ./.env ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

# EXPOSE 3002

# In order to be able to issue and sign Verifiable credentials you need to create a DID.  
CMD [ "npm", "run","create_identity" ]

# In order to run the service:
CMD [ "npm", "run","dev" ]