# Schluss Identity Service

With this Identity service it becomes possible to: 
- verify an email address (more attribute types in the near future)
- issue a Verifiable Credential and hand it out to a Holder
- check the validity of a  Verifiable Presentation by a Verifier


## Run locally
For local running, testing, and development follow instructions below:

### Requirements
- NodeJS

#### Configuration:   
Rename .env.example to .env and modify the configuration parameters.

#### Create an Identity
In order to be able to issue and sign Verifiable credentials you need to create a DID.  
> npm run create_identity   

The script generates a keypair and publishes it on the ledger, giving a did in return. Using these this service becomes an Issuer. Make sure you enter these at ISSUER_PUBLIC, ISSUER_SECRET and ISSUER_DID in the .env file.

#### Run the service
In order to run the service:
> npm run dev

## Demonstration of the flow
It is possible to see / test the whole flow by running the client_example. This does the following:
- Send an email address to the Schluss Identity Service
- The service sends a verification email to the supplied email address
- After verification, the service issues a new Verifiable credential and sends it back
- The credential is then made into a Verifiable presentation and 'shared' with a third party
- The third party verifies the validity of the presentation using the service

Make sure you input your own email address at /examples/client.js at EMAIL_ADDRESS. Then run the service, and finally run this demonstration as follows:   
> npm run client_example

# Running via Docker
Docker allows you to run the server without having Node.js installed. Before you can run the docker container make sure you carry out the steps described at [Configuration](#configuration) and [Create an Identity](#create-an-identity) to set up the service.

###  Docker build with tag : schluss-identity-service 
For more options please see https://docs.docker.com/engine/reference/commandline/build/

`docker build -t schluss-identity-service . `

###  Docker run 

-i = --interactive ( Keep STDIN open even if not attached) \
-t = --tty ( Allocate a pseudo-TTY ) \
-d = --detach ( run container in the background) \

For more options please see https://docs.docker.com/engine/reference/commandline/run/ 

`docker run -itd -p <Host Port>:3002 schluss-identity-service `

###  Docker list active containers 
`docker container ls`

### Docker stop active containers ( Container ID Retrieved from container list ) 
`docker stop <Container ID>`

For more Docker Options see https://docs.docker.com/reference/
